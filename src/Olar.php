<?php

namespace Diogoko\Biblioteca;

class Olar
{
	private $versao = '1.2.0';

	public $maiusculas = false;

	public function diga($nome, $pergunta = false)
	{
		$pontuacao = $pergunta ? '?' : '!';
		$texto = "Olar, {$nome}{$pontuacao} (versão {$this->versao})";
		if ($this->maiusculas) {
			$texto = mb_strtoupper($texto);
		}
		return $texto;
	}
}

